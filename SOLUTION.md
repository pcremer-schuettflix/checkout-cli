# Solution

What to look out for.

## Version 1

### Good

* Proper use of `let` and `const`
* Use of [`Intl.NumberFormat`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/NumberFormat) for transforming integer prices to currency strings
* Uses `Array.prototype.map` and `Array.prototype.filter` to create `lineItems` array
* Uses `Array.prototype.reduce` to calculate `subtotal`
* Uses modern language features such as
    * Arrow functions
    * Shorthand object properties
    * Object destructuring
* Looks at the test output

### Doubt

* Writes own function for transforming integer prices to currency strings
* Tries weird things to calculate a sum using currency strings and `parseFloat()`
* Use of unnecessary third party modules
* Uses `var` in 2021

## Version 2

### Good

* Creates own module for currency formatter to avoid duplication
    * Makes it generic for other locales and currencies
* Uses `Promise.all` (live: with a little help/nudge) to fetch products
    * Uses `Array.prototype.map` to map product IDs to promises

### Better

* [Live] Immediately selects `Promise.all` to fetch products without Googling
* Already implements error handling for rejected promises, either via try-catch or `Promise.allSettled`

### Doubt

* Uses `await` to fetch products successively (won't pass test timeout!)
* Generally struggles with the concept of asynchronous functions
* Does not know promises

## Bonus Challenge (Senior Engineers)

### Good

* [Live] Can solve the task with (little) supervision and hints
* [Live] Knows how to use basic Jest functionality without Googling

### Better

* Uses `mockClear()` to clean up between tests
* [Live] Can solve task enirely on their own
* Is not a senior, but solves the task nevertheless (w/ or w/o hints)

### Doubt

* Does not even try to solve this task
* Tries weird and unconventional things to mock the dependency
* [Live] Struggles with basic testing concepts (e.g. what are unit tests? what are mocks?)
* [Live] Strugges with basic Jest functionality and has to Google a lot