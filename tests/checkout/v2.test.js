import { expect, describe, it } from '@jest/globals';
import { invoice } from '../../src/checkout/v2';
import * as fetch from '../../src/fetch';
jest.mock('../../src/fetch');

beforeEach(() => {
  fetch.get.mockClear();
});

const mockCatalog = [
  { id: '0001', name: 'Granit Schotter', price: 500 },
  { id: '0002', name: 'Quarzsand', price: 690 },
  { id: '0003', name: 'Rindenmulch', price: 250 },
  { id: '0005', name: 'Basalt', price: 720 },
];

describe('invoice', () => {
  describe('valid order', () => {
    beforeEach(() => {
      mockCatalog.forEach(product => 
        fetch.get.mockResolvedValueOnce(product));
    });

    it('should run without problems', async () => {
      const expectedLineItems = [
        { id: '0001', name: 'Granit Schotter', total: '50,00 €' },
        { id: '0002', name: 'Quarzsand', total: '103,50 €' },
        { id: '0003', name: 'Rindenmulch', total: '50,00 €' },
        { id: '0005', name: 'Basalt', total: '180,00 €' }
      ];

      const inv = await invoice();
      expect(inv.lineItems).toStrictEqual(expectedLineItems);
      expect(inv.subtotal).toEqual('383,50 €');
      expect(inv.vat).toEqual('72,87 €');
      expect(inv.overall).toEqual('456,37 €');
    }, 3000);
  });

  describe('order with unknown id', () => {
    beforeEach(() => {
      fetch.get.mockResolvedValueOnce(mockCatalog[0]);
      fetch.get.mockResolvedValueOnce(mockCatalog[1]);
      fetch.get.mockRejectedValueOnce('unknown id');
      fetch.get.mockResolvedValueOnce(mockCatalog[3]);
    });

    it('should dismiss the invalid order id', async () => {
      const expectedLineItems = [
        { id: '0001', name: 'Granit Schotter', total: '50,00 €' },
        { id: '0002', name: 'Quarzsand', total: '103,50 €' },
        { id: '0005', name: 'Basalt', total: '180,00 €' }
      ];

      const inv = await invoice();
      expect(inv.lineItems).toStrictEqual(expectedLineItems);
      expect(inv.subtotal).toEqual('333,50 €');
      expect(inv.vat).toEqual('63,37 €');
      expect(inv.overall).toEqual('396,87 €');
    }, 3000);
  });
});