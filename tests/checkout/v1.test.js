import { expect, test } from '@jest/globals';
import { invoice } from '../../src/checkout/v1';

const expectedLineItems = [
  { id: '0001', name: 'Granit Schotter', total: '50,00 €' },
  { id: '0002', name: 'Quarzsand', total: '103,50 €' },
  { id: '0003', name: 'Rindenmulch', total: '50,00 €' },
  { id: '0005', name: 'Basalt', total: '180,00 €' }
];

test('invoice', () => {
  expect(invoice.lineItems).toStrictEqual(expectedLineItems);
  expect(invoice.subtotal).toEqual('383,50 €');
  expect(invoice.vat).toEqual('72,87 €');
  expect(invoice.overall).toEqual('456,37 €');
});