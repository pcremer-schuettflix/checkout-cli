import { displayPrice } from '../util.js';

const basket = {
  customer: {
    id: '0001',
    name: 'Alice',
    vat: '19%',
  },
  items: [
    {
      product: { id: '0001', name: 'Granit Schotter', price: 500 },
      qty: 10
    },
    {
      product: { id: '0002', name: 'Quarzsand', price: 690 },
      qty: 15
    },
    {
      product: { id: '0003', name: 'Rindenmulch', price: 250 },
      qty: 20
    },
    {
      product: { id: '0004', name: 'Betonkies', price: 350 },
      qty: 0
    },
    {
      product: { id: '0005', name: 'Basalt', price: 720 },
      qty: 25
    },
  ]
};

const lineItems = basket.items
  .filter(item => item.qty > 0)
  .map(item => {
    const { id, name, price } = item.product;
    return {
      id,
      name,
      total: displayPrice(price*item.qty)
    };
  }
);

const subtotal = basket.items.reduce((acc, item) => {
  return acc + item.product.price*item.qty;
}, 0);

const vat = subtotal*(parseInt(basket.customer.vat)/100);

export const invoice = {
  lineItems,
  subtotal: displayPrice(subtotal),
  vat: displayPrice(vat),
  overall: displayPrice(subtotal + vat),
};