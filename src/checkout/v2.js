// Hint: `fetch#get` is an async function, which simulates a REST API.
// It returns an product object for a given id. If an id does not exist, 
// an error will be thrown.
import * as fetch from '../fetch.js';
import { displayPrice } from '../util.js';

const customer = { 
  id: '0002', 
  name: 'Bob', 
  vat: '19%' 
};

const order = {
  '0003': 20,
  '0005': 25,
  '0002': 15,
  '0001': 10,
};

export const invoice = async () => {
  const promises = Object
    .keys(order)
    .sort()
    .map(productId => fetch.get(productId));

  const fulfilledItems = (await Promise
    .allSettled(promises))
    .filter(result => result.status === 'fulfilled')
    .map(result => result.value);

  const lineItems = fulfilledItems.map(item => {
      const { id, name, price } = item;
      return {
        id,
        name,
        total: displayPrice(price*order[id])
      }
    }
  );

  const subtotal = fulfilledItems.reduce((acc, item) => {
    return acc + item.price*order[item.id];
  }, 0);
  
  const vat = subtotal*(parseInt(customer.vat)/100);

  return { 
    lineItems,
    subtotal: displayPrice(subtotal),
    vat: displayPrice(vat),
    overall: displayPrice(subtotal + vat),
  };
};