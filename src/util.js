export const displayPrice = (cents, locale = 'de-DE', currency = 'EUR') =>
  new Intl.NumberFormat(locale, { style: 'currency', currency }).format(cents/100);